<div align="center">
    <img alt="big-poop logo" src="https://gitlab.com/poop.person/poop-project/-/raw/master/big-poop.svg">
</div>


# POOP: Peacefully Opposing Oppressive Puritanism

POOP is a badge for any software project that is [under attack by modern day puritans][opalgate]. Use this badge to declare that your project is POOP: Peacefully Opposing Oppressive Puritanism.

POOP does not imply an endorsement of any philosophy, ideology, religion or politics, it is simply a recognizable and whimsical but firm way to answer "NO" to demands such as these:

- Censor contributors, especially for behaviour that does not involve the project.
- Apologize or give penance for perceived moral transgressions, especially those involving nothing more than words.
- Adopt a code of conduct or practice any other form of self-flagellation.
- Preferentially accept contributions based on the group identity of a contributor.

Needless to say, POOP does not imply many things. To list these would be a form of genuflection, so they will remain unsaid. Any accusation of bigotry does not deserve an explanation, it should be met with POOP.


## POOP for Freedom

The POOP badge exists for the same reason as the [Harpers - Letter on Justice and Open Debate][harpers_letter] which describes the problem well:

> The free exchange of information and ideas, the lifeblood of a liberal society, is daily becoming more constricted. While we have come to expect this on the radical right, censoriousness is also spreading more widely in our culture: an intolerance of opposing views, a vogue for public shaming and ostracism, and the tendency to dissolve complex policy issues in a blinding moral certainty. We uphold the value of robust and even caustic counter-speech from all quarters. But it is now all too common to hear calls for swift and severe retribution in response to perceived transgressions of speech and thought.
>
> [...]
>
> Whatever the arguments around each particular incident, the result has been to steadily narrow the boundaries of what can be said without the threat of reprisal. We are already paying the price in greater risk aversion among writers, artists, and journalists who fear for their livelihoods if they depart from the consensus, or even lack sufficient zeal in agreement.

Programmers and maintainers have been exposed to this kind of risk for some years and [have suffered][google_echo_chamber] [consequences][brendan_eich] for [nothing more][stallman_fsf_resignation] [than words][donglegate] [they have said][linus_stfu]. Without wishing to endorse (or renounce) any of the examples linked, it is clear that the puritan mob can have a significant negative impact on the professional career of anybody who steps out of line. It is unreasonable to expect that everybody in the world will agree with each other and it is a form of oppression when dogmatic, illiberal activists can make people kowtow to their demands while their opposition is silenced.


## The POOP Badge

If you would like to use the POOP badge on your project, add either of these codes to your README.md:

```markdown
[![POOP badge](https://img.shields.io/badge/POOP-%F0%9F%92%A9-fff)](https://gitlab.com/poop.person/poop-project/-/blob/master/README.md)
```

```html
<a href="https://gitlab.com/poop.person/poop-project/-/blob/master/README.md"><img alt="POOP badge" src="https://img.shields.io/badge/POOP-%F0%9F%92%A9-fff"></a>
```

If you would like to served the images yourself, you can use these downloads:

- [![POOP badge](https://img.shields.io/badge/POOP-%F0%9F%92%A9-fff)](https://gitlab.com/poop.person/poop-project/-/blob/master/README.md)

- [![POOP badge](https://gitlab.com/poop.person/poop-project/-/raw/master/poop-badge.svg)](https://gitlab.com/poop.person/poop-project/-/blob/master/README.md)



---------------------------------------

[opalgate]: https://github.com/opal/opal/issues/941

[harpers_letter]: https://harpers.org/a-letter-on-justice-and-open-debate/

[google_echo_chamber]: https://assets.documentcloud.org/documents/3914586/Googles-Ideological-Echo-Chamber.pdf

[brendan_eich]: https://en.wikipedia.org/wiki/Brendan_Eich#Appointment_to_CEO,_Proposition_8_Controversy,_and_Resignation

[donglegate]: https://news.ycombinator.com/item?id=5391667

[stallman_fsf_resignation]: https://stallman.org/archives/2019-jul-oct.html#24_September_2019_(FSF)

[linus_stfu]: https://lkml.org/lkml/2012/12/23/75

